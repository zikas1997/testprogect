package ru.testprogect.counter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.testprogect.counter.service.TimeLastDayService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CounterApplicationTests {

    @Autowired
    TimeLastDayService timeLastDayService;

    @Test
    public void contextLoads() {
        timeLastDayService.getDateLastDay();
    }

}
