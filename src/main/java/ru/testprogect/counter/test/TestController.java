package ru.testprogect.counter.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.testprogect.counter.json.model.VisitDay;

@Controller
public class TestController {

    @Autowired
    TestGeneratorEvent testGeneratorEvent;

    @RequestMapping("/run")
    @ResponseBody
    public String statisticVisitUserByLastDay(){
        testGeneratorEvent.generate();
        return "Ready!";
    }
}
