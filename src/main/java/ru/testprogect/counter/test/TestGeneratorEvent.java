package ru.testprogect.counter.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.testprogect.counter.service.EventHandlerService;

@Component
public class TestGeneratorEvent {

    @Autowired
    EventHandlerService eventHandlerService;

    public void generate() {
        for(int i =0; i<15; i++){
            eventHandlerService.event("192.168.54.1","page1");
            eventHandlerService.event("192.168.54.2","page12");
            eventHandlerService.event("192.168.54.3","page13");
        }
        for(int i =0; i<4; i++){
            eventHandlerService.event("192.168.54.1","page1");
            eventHandlerService.event("192.168.54.1","page12");
            eventHandlerService.event("192.168.54.1","page13");
        }
        for(int i =0; i<8; i++){
            eventHandlerService.event("172.168.54.2","page1");
            eventHandlerService.event("172.168.54.2","page1");
            eventHandlerService.event("172.168.54.2","page1");
        }

    }
}
