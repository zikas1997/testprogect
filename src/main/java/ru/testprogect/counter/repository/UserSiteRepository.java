package ru.testprogect.counter.repository;

import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.testprogect.counter.model.UserSite;

public interface UserSiteRepository extends JpaRepository<UserSite,Long> {
    UserSite findByIpUser(String ip);
}
