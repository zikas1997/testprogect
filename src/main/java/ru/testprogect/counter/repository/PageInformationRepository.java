package ru.testprogect.counter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.testprogect.counter.model.PageInformation;

import java.util.List;

public interface PageInformationRepository extends JpaRepository<PageInformation,Long> {
    PageInformation findByNamePage(String namePage);
    List<PageInformation> findAllByNamePage(String namePage);
}
