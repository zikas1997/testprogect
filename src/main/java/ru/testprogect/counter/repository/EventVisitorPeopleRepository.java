package ru.testprogect.counter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.testprogect.counter.model.EventVisitorPeople;
import ru.testprogect.counter.model.PageInformation;

import java.util.Date;
import java.util.List;

public interface EventVisitorPeopleRepository extends JpaRepository <EventVisitorPeople,Long>{
    List<EventVisitorPeople> findAllByPageInformationAndDateBetween(PageInformation pageInformation,
                                                                    Date dateBegin,Date date2);
    List<EventVisitorPeople> findAllByDateBetween(Date dateBegin,Date date2);
}
