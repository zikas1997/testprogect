package ru.testprogect.counter.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.testprogect.counter.model.UserSite;

@Component
public class UserSiteRepositoryImplement {

    final UserSiteRepository userSiteRepository;

    @Autowired
    public UserSiteRepositoryImplement(UserSiteRepository userSiteRepository) {
        this.userSiteRepository = userSiteRepository;
    }

    public UserSite save(String ip) {
        UserSite us = userSiteRepository.findByIpUser(ip);
        if (us == null)
            return userSiteRepository.save(new UserSite(ip));
        return us;
    }
}
