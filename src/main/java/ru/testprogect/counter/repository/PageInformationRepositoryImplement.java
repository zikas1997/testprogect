package ru.testprogect.counter.repository;

import org.springframework.stereotype.Component;
import ru.testprogect.counter.model.PageInformation;

@Component
public class PageInformationRepositoryImplement {

    private final PageInformationRepository pageInformationRepository;

    public PageInformationRepositoryImplement(PageInformationRepository pageInformationRepository) {
        this.pageInformationRepository = pageInformationRepository;
    }

    public PageInformation save(String namePage) {
        PageInformation pageInformation = pageInformationRepository.findByNamePage(namePage);
        if(pageInformation == null)
            return pageInformationRepository.save(new PageInformation(namePage));
        return pageInformation;
    }
}
