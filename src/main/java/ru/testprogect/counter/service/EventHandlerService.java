package ru.testprogect.counter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.testprogect.counter.model.EventVisitorPeople;
import ru.testprogect.counter.model.PageInformation;
import ru.testprogect.counter.model.UserSite;
import ru.testprogect.counter.repository.PageInformationRepositoryImplement;
import ru.testprogect.counter.repository.UserSiteRepositoryImplement;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

@Component
public class EventHandlerService {

    private final UserSiteRepositoryImplement saveUser;
    private final EventCreatorService eventCreatorService;
    private final PageInformationRepositoryImplement pageInformationRepositoryImplement;

    @Autowired
    public EventHandlerService(UserSiteRepositoryImplement saveUser, EventCreatorService eventCreatorService, PageInformationRepositoryImplement pageInformationRepositoryImplement) {
        this.saveUser = saveUser;
        this.eventCreatorService = eventCreatorService;
        this.pageInformationRepositoryImplement = pageInformationRepositoryImplement;
    }

    public void event(String ip, String page) {
        UserSite userSite = saveUser.save(ip);
        PageInformation pageInformation = pageInformationRepositoryImplement.save(page);
        EventVisitorPeople eventVisitorPeople = eventCreatorService.create(userSite, pageInformation, new Date());
    }
}
