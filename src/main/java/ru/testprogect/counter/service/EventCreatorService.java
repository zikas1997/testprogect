package ru.testprogect.counter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.testprogect.counter.model.EventVisitorPeople;
import ru.testprogect.counter.model.PageInformation;
import ru.testprogect.counter.model.UserSite;
import ru.testprogect.counter.repository.EventVisitorPeopleRepository;
import ru.testprogect.counter.repository.PageInformationRepository;
import ru.testprogect.counter.repository.UserSiteRepository;

import java.util.Date;

@Component
public class EventCreatorService {

    private final EventVisitorPeopleRepository eventVisitorPeopleRepository;
    private final PageInformationRepository pageInformationRepository;
    private final UserSiteRepository userSiteRepository;

    @Autowired
    public EventCreatorService(EventVisitorPeopleRepository eventVisitorPeopleRepository, PageInformationRepository pageInformationRepository, UserSiteRepository userSiteRepository) {
        this.eventVisitorPeopleRepository = eventVisitorPeopleRepository;
        this.pageInformationRepository = pageInformationRepository;
        this.userSiteRepository = userSiteRepository;
    }

    public EventVisitorPeople create(UserSite userSite, PageInformation pageInformation, Date date){
        return eventVisitorPeopleRepository.save(new EventVisitorPeople(userSite,pageInformation, date));
    }
}
