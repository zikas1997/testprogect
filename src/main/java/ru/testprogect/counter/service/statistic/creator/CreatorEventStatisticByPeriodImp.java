package ru.testprogect.counter.service.statistic.creator;

import org.springframework.stereotype.Component;
import ru.testprogect.counter.json.model.VisitPeriod;
import ru.testprogect.counter.model.EventVisitorPeople;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CreatorEventStatisticByPeriodImp {

    public CreatorEventStatisticByPeriodImp() {
    }

    public VisitPeriod create(List<EventVisitorPeople> eventVisitorPeople){
        VisitPeriod visitDay = new VisitPeriod();
        visitDay.setTotalUserCount(eventVisitorPeople.size());
        visitDay.setUniqueUserCount(
                (int)eventVisitorPeople.stream()
                        .collect(Collectors.groupingBy(o -> o.getUserSite().getIdUser(),Collectors.counting()))
                        .entrySet().stream()
                        .filter(stringLongEntry -> stringLongEntry.getValue()>0)
                        .count()
        );
        visitDay.setRegularUserCount(
                (int)eventVisitorPeople.stream()
                        .collect(Collectors.groupingBy(o -> o.getUserSite().getIdUser(),Collectors.counting()))
                        .entrySet().stream()
                        .filter(stringLongEntry -> stringLongEntry.getValue()>9)
                        .count()
        );
        return visitDay;
    }
}
