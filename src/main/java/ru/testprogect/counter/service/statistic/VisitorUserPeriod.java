package ru.testprogect.counter.service.statistic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.testprogect.counter.json.model.VisitPeriod;
import ru.testprogect.counter.model.EventVisitorPeople;
import ru.testprogect.counter.repository.EventVisitorPeopleRepository;
import ru.testprogect.counter.repository.PageInformationRepository;
import ru.testprogect.counter.service.TimeLastDayService;
import ru.testprogect.counter.service.statistic.creator.CreatorEventStatisticByPeriodImp;

import java.util.Date;
import java.util.List;

@Component
public class VisitorUserPeriod {

    private final EventVisitorPeopleRepository eventVisitorPeopleRepository;
    private final TimeLastDayService timeLastDayService;
    private final CreatorEventStatisticByPeriodImp creatorEventStatisticByPeriodImp;

    @Autowired
    public VisitorUserPeriod(EventVisitorPeopleRepository eventVisitorPeopleRepository, TimeLastDayService timeLastDayService, PageInformationRepository pageInformationRepository, CreatorEventStatisticByPeriodImp creatorEventStatisticByPeriodImp) {
        this.eventVisitorPeopleRepository = eventVisitorPeopleRepository;
        this.timeLastDayService = timeLastDayService;
        this.creatorEventStatisticByPeriodImp = creatorEventStatisticByPeriodImp;
    }

    public VisitPeriod statistic(Date date1, Date date2) {
        List<EventVisitorPeople> allEvent = eventVisitorPeopleRepository.findAllByDateBetween(date1, date2);
        return creatorEventStatisticByPeriodImp.create(allEvent);
    }
}
