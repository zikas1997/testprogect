package ru.testprogect.counter.service.statistic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.testprogect.counter.json.model.VisitDay;
import ru.testprogect.counter.model.EventVisitorPeople;
import ru.testprogect.counter.model.PageInformation;
import ru.testprogect.counter.repository.EventVisitorPeopleRepository;
import ru.testprogect.counter.repository.PageInformationRepository;
import ru.testprogect.counter.service.TimeLastDayService;
import ru.testprogect.counter.service.statistic.creator.CreatorEventStatisticLastDayImp;

import java.util.Date;
import java.util.List;

@Component
public class VisitorUserLastDay {

    private final EventVisitorPeopleRepository eventVisitorPeopleRepository;
    private final TimeLastDayService timeLastDayService;
    private final PageInformationRepository pageInformationRepository;
    private final CreatorEventStatisticLastDayImp creatorEventStatisticLastDayImp;

    @Autowired
    public VisitorUserLastDay(EventVisitorPeopleRepository eventVisitorPeopleRepository, TimeLastDayService timeLastDayService, PageInformationRepository pageInformationRepository, CreatorEventStatisticLastDayImp creatorEventStatisticLastDayImp) {
        this.eventVisitorPeopleRepository = eventVisitorPeopleRepository;
        this.timeLastDayService = timeLastDayService;
        this.pageInformationRepository = pageInformationRepository;
        this.creatorEventStatisticLastDayImp = creatorEventStatisticLastDayImp;
    }

    public VisitDay statistic(String page){
        PageInformation pageInformation = pageInformationRepository.findByNamePage(page);
        List<EventVisitorPeople> allEvent = eventVisitorPeopleRepository.findAllByPageInformationAndDateBetween(pageInformation, timeLastDayService.getDateLastDay(), new Date());
        return creatorEventStatisticLastDayImp.create(allEvent);
    }
}
