package ru.testprogect.counter.service.statistic.creator;

import org.springframework.stereotype.Component;
import ru.testprogect.counter.json.model.VisitDay;
import ru.testprogect.counter.model.EventVisitorPeople;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CreatorEventStatisticLastDayImp{

    public CreatorEventStatisticLastDayImp() {
    }

    public VisitDay create(List<EventVisitorPeople> eventVisitorPeople){
        VisitDay visitDay = new VisitDay();
        visitDay.setTotalUserCount(eventVisitorPeople.size());
        visitDay.setUniqueUserCount(
                (int)eventVisitorPeople.stream()
                        .collect(Collectors.groupingBy(o -> o.getUserSite().getIdUser(),Collectors.counting()))
                        .entrySet().stream()
                        .filter(stringLongEntry -> stringLongEntry.getValue()>0)
                        .count()
        );
        return visitDay;
    }
}
