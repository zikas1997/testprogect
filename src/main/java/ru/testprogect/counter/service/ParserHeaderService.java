package ru.testprogect.counter.service;

import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class ParserHeaderService {

    public String getIpClient(HttpServletRequest request){
         return request.getRemoteAddr();
    }
}
