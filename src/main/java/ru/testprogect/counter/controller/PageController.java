/*
package ru.testprogect.counter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.testprogect.counter.service.EventHandlerService;

import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@Controller
public class PageController {

    private final EventHandlerService eventHandlerService;

    @Autowired
    public PageController(EventHandlerService eventHandlerService) {
        this.eventHandlerService = eventHandlerService;
    }

    @RequestMapping(value = "/page1", method = RequestMethod.GET)
    public  ResponseEntity<String> page1(HttpServletRequest request){
        eventHandlerService.event(request,"page1");
        return new ResponseEntity<>("Hello word!", HttpStatus.OK);
    }

    @RequestMapping(value = "/page2", method = RequestMethod.GET)
    public  ResponseEntity<String> page2(HttpServletRequest request){
        eventHandlerService.event(request,"page2");
        return new ResponseEntity<>("Hello developer!", HttpStatus.OK);
    }
    @RequestMapping(value = "/page3", method = RequestMethod.GET)
    public  ResponseEntity<String> page3(HttpServletRequest request){
        eventHandlerService.event(request,"page3");
        return new ResponseEntity<>("Hello my friend!", HttpStatus.OK);
    }

}
*/
