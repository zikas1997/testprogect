package ru.testprogect.counter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.testprogect.counter.json.convector.JsonSerialize;
import ru.testprogect.counter.json.model.VisitDay;
import ru.testprogect.counter.json.model.VisitPeriod;
import ru.testprogect.counter.service.EventHandlerService;
import ru.testprogect.counter.service.TimeLastDayService;
import ru.testprogect.counter.service.statistic.VisitorUserLastDay;
import ru.testprogect.counter.service.statistic.VisitorUserPeriod;

import java.util.Date;

@Controller
public class StatisticController {

    private final EventHandlerService eventHandlerService;
    private final VisitorUserPeriod visitorUserPeriod;
    private final VisitorUserLastDay visitorUserLastDay;
    private final JsonSerialize jsonSerialize;
    private final TimeLastDayService timeLastDayService;

    @Autowired
    public StatisticController(EventHandlerService eventHandlerService, VisitorUserPeriod visitorUserPeriod, VisitorUserLastDay visitorUserLastDay, JsonSerialize jsonSerialize, TimeLastDayService timeLastDayService) {
        this.eventHandlerService = eventHandlerService;
        this.visitorUserPeriod = visitorUserPeriod;
        this.visitorUserLastDay = visitorUserLastDay;
        this.jsonSerialize = jsonSerialize;
        this.timeLastDayService = timeLastDayService;
    }

    @RequestMapping("/statisticVisitUserByLastDay")
    @ResponseBody
    public String statisticVisitUserByLastDay(@RequestParam String ipUser,
                                              @RequestParam String page){
        eventHandlerService.event(ipUser,page);
        VisitDay visitDay = visitorUserLastDay.statistic(page);
        return jsonSerialize.serialize(visitDay);
    }

    @RequestMapping("/statisticVisitUserByPeriodDate")
    @ResponseBody
    public String statisticVisitUserByPeriodDate(@RequestParam String fitsDate,
                                              @RequestParam String secondDate){
        Date date1 =timeLastDayService.getDate(fitsDate);
        Date date2 = timeLastDayService.getDate(secondDate);
        if(date1== null&& date2==null){
            return "Date exception! Please write date format: dd.mm.yyyy (15.03.1997). Also, first date was more second date.";
        }
        VisitPeriod visitPeriod = visitorUserPeriod.statistic(date1, date2);
        return jsonSerialize.serialize(visitPeriod);
    }
}
