package ru.testprogect.counter.model;

import lombok.*;

import javax.persistence.*;

import java.util.Set;

@Entity
@AllArgsConstructor
public class UserSite {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idUser;
    private String ipUser;
    @OneToMany(mappedBy = "userSite")
    private Set<EventVisitorPeople> eventVisitorPeople;

    public UserSite() {
    }

    public UserSite(String ipUser) {
        this.ipUser = ipUser;
    }

    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    public String getIpUser() {
        return ipUser;
    }

    public void setIpUser(String ipUser) {
        this.ipUser = ipUser;
    }

    public Set<EventVisitorPeople> getEventVisitorPeople() {
        return eventVisitorPeople;
    }

    public void setEventVisitorPeople(Set<EventVisitorPeople> eventVisitorPeople) {
        this.eventVisitorPeople = eventVisitorPeople;
    }
}
