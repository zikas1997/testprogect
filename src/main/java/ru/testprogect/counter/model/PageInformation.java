package ru.testprogect.counter.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;


@Getter
@Setter
@Entity
@AllArgsConstructor
public class PageInformation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idPage;
    private String namePage;
    @OneToMany(mappedBy = "pageInformation")
    private Set<EventVisitorPeople> eventVisitorPeople;

    public PageInformation() {
    }

    public PageInformation(String namePage) {
        this.namePage = namePage;
    }
}
