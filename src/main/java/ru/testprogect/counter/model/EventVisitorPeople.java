package ru.testprogect.counter.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;


@Entity
@AllArgsConstructor
public class EventVisitorPeople {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idEvent;
    @ManyToOne()
    private UserSite userSite;
    @ManyToOne()
    private PageInformation pageInformation;
    private Date date;

    public EventVisitorPeople() {
    }

    public EventVisitorPeople(UserSite userSite, PageInformation pageInformation, Date date) {
        this.userSite = userSite;
        this.pageInformation = pageInformation;
        this.date = date;
    }

    public long getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(long idEvent) {
        this.idEvent = idEvent;
    }

    public UserSite getUserSite() {
        return userSite;
    }

    public void setUserSite(UserSite userSite) {
        this.userSite = userSite;
    }

    public PageInformation getPageInformation() {
        return pageInformation;
    }

    public void setPageInformation(PageInformation pageInformation) {
        this.pageInformation = pageInformation;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
