package ru.testprogect.counter.json.convector;

import com.google.gson.Gson;
import org.springframework.stereotype.Component;

@Component
public class JsonSerialize {

    private Gson gson;

    public JsonSerialize() {
        gson = new Gson();
    }

    public <T> String serialize(T obj) {
        return gson.toJson(obj);
    }
}
