package ru.testprogect.counter.json.model;

public class VisitDay {

    private int totalUserCount;
    private int uniqueUserCount;

    public VisitDay() {
    }

    public int getTotalUserCount() {
        return totalUserCount;
    }

    public void setTotalUserCount(int totalUserCount) {
        this.totalUserCount = totalUserCount;
    }

    public int getUniqueUserCount() {
        return uniqueUserCount;
    }

    public void setUniqueUserCount(int uniqueUserCount) {
        this.uniqueUserCount = uniqueUserCount;
    }
}
