package ru.testprogect.counter.json.model;

public class VisitPeriod {

    private int totalUserCount;
    private int uniqueUserCount;
    private int regularUserCount;

    public int getTotalUserCount() {
        return totalUserCount;
    }

    public void setTotalUserCount(int totalUserCount) {
        this.totalUserCount = totalUserCount;
    }

    public int getUniqueUserCount() {
        return uniqueUserCount;
    }

    public void setUniqueUserCount(int uniqueUserCount) {
        this.uniqueUserCount = uniqueUserCount;
    }

    public int getRegularUserCount() {
        return regularUserCount;
    }

    public void setRegularUserCount(int regularUserCount) {
        this.regularUserCount = regularUserCount;
    }
}
